
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Content 1.3
import QtWebEngine 1.7
import QtQuick.XmlListModel 2.0
import Ubuntu.Connectivity 1.0
import Ubuntu.OnlineAccounts 2.0

import "Components/UI"
import "Components/ContentHub"
import "Components"
import "Pages"
import "helpers"
import "Jslibs/nextCloudAPI.js" as NextcloudAPI
import HelperFunctions 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'darkeye.ursses'
    automaticOrientation: false
	anchorToKeyboard: true
	
	LayoutMirroring.enabled: false
    LayoutMirroring.childrenInherit: true

    width: units.gu(45)
    height: units.gu(75)

	theme.name: ""
    backgroundColor: theme.palette.normal.background
	property var urls :  [];
	property alias mainBillboard: mainEventBillboard
	property alias accountModel: accountsModel
	property var nextCloudCreds: null;
	property bool applicationState: Qt.application.active

	onApplicationStateChanged: if(applicationState) {
		console.log("Updating theme to :"+Theme.name);
		root.theme.name = Theme.name;
	}
	
	onUrlsChanged : if(root.urls.length) {
		console.log("onUrlsChanged")
		mainBillboard.feedsChanged(root.urls);
		mainFeed.updateFeed();
		urssesMainDB.syncUrls();
	}
	
	Settings {
		id:appSettings
		property bool showDescInsteadOfWebPage: false
		property bool showSections: false
		property string mainFeedSectionField : "channel"
		property alias urls: root.urls
		property var bookedmarked : []
		property int itemsToLoadPerChannel : 7
		property int mainFeedSortAsc :Qt.DescendingOrder
		property string mainFeedSortField : "updated"
		property real webBrowserDefaultZoom : 1.0
		property int updateFeedEveryXMinutes : 5
		property bool openFeedsExternally: false
		property bool swipeBetweenEntries: true
		property int feedCacheTimeout: 10
		property real webViewBaseOpacity: 1.0
		property var nextCloudCreds :{"host":"","user":"","pass":"","accountId":false}
		property int storyReadTime: 5;
	}
	
	FeedCache {
		id:feedCacheDB
	}
	
	SimpleDB {
		id:urssesMainDB
		function syncUrls() {
			var dbUrls = urssesMainDB.getDocs();
			if(root.urls && root.urls.length) {
				if( (!dbUrls || !dbUrls.length || root.urls.length > dbUrls.length )  ) {
					console.log("Updating DB.");
					for(var idx in root.urls) {
						if(!urssesMainDB.getDoc( root.urls[idx])) {
							urssesMainDB.putDoc( root.urls[idx]);
						}
					}
				}
			} else if(dbUrls.length && !root.urls.length ) {
				root.urls = urssesMainDB.getDocs();
			}
		}
	}
	
	CachedHttpRequest {
		id:cachedHttpRequestInstance
		isResultJSON : false;
		logActions:false;
		onlyRetrunFreshCache:Connectivity.status !== NetworkingStatus.Offline ;
		cachingTimeMiliSec: appSettings.feedCacheTimeout * 60000;
		recachingFactor: 0.25;
		cachedResponseIsEnough:true
	}
	
	WebEngineProfile {
			id:webProfileInstace
			persistentCookiesPolicy: WebEngineProfile.AllowPersistentCookies
			storageName: "ursses"
			httpCacheType: WebEngineProfile.DiskHttpCache
	}
		
	EventBillboard {
		id:mainEventBillboard
		onBookmarkStory :{
			appSettings.bookedmarked.push(story);
			appSettings.bookedmarked = appSettings.bookedmarked;
		}
		onUnbookmarkStory :{
			var index = root.getBookMarkedIdx(story.url);
			if(index >= 0) {
				appSettings.bookedmarked.splice(index,1);
				appSettings.bookedmarked = appSettings.bookedmarked;
			}
		}
		onRemoveFeed:{
			for(var index=0; index< root.urls.length;index++) {
				if(feedUrl == root.urls[index]) {
					root.urls.splice(index,1);
					break;
				}
			}
			root.urls = root.urls;
		}
		onAddFeed: {
			if(root.urls.indexOf(feedUrl) < 0) {
				root.urls.push( feedUrl );
				root.urls = root.urls;
			}
		}
		onExportFeeds: {
			console.log("Exporting feeds");
			var exportFile = "/home/phablet/.local/share/darkeye.ursses/feeds_list.opml";
			var feedList = [];
			for(var url of feedsToExport) {
				var channel = mainFeed.getChannelByElmMatch({feedUrl:url});
				feedList.push({"title":channel.titleText,"text":channel.titleText,"description":channel.description,"xmlUrl":url,"type":(channel['isAtom'] ? "atom": "rss")});
			}
			//console.log(JSON.stringify(feedList, null, '\t'));
			if(helperFunctions.exportToFile(exportFile,helperFunctions.generateOPMLFromList(feedList) )) {
				shareItem.shareFile(Qt.resolvedUrl(exportFile));
			}
		}
		onExportBookmarks: {
			console.log("Exporting Bookmarks");
			var exportFile = "/home/phablet/.local/share/darkeye.ursses/bookmarks_to_share.html";
			if(helperFunctions.exportToFile(exportFile, articlesToExport)) {
				shareItem.shareFile(Qt.resolvedUrl(exportFile));
			}
		}
	}
	
	AccountModel {
		id: accountsModel
		function authenticate(authJsonData) {
			for(var idx in accountsModel.accountList) {
				var acct = accountsModel.accountList[idx];
				console.log(JSON.stringify(acct));
				if(acct.accountId == authJsonData.accountId && acct.valid) {
					acct.authenticationReply.connect(function(reply){
						if (reply.errorCode) {
							console.log("Failed to obtain access to nextcloud got error : "+reply.errorText);
							root.primaryPageNotify(i18n.tr("Failed to obtain access to nextcloud got error : %1").args(reply.errorText));
						} else {
							root.syncNextCloud(Qt.btoa(reply.Username+":"+reply.Password));
						};
					});
					acct.authenticate({host:authJsonData.host,accountId:authJsonData.accountId});
					console.log("Found NextCloud account :"+ authJsonData.accountId);
					return;
				}
			}
			console.log("No NextCloud account found for Account ID :"+ authJsonData.accountId);
		}
	}
	
	//-------------------------------------------- Events behavior ----------------------------------

	Component.onCompleted:  {
		if(Qt.application.arguments) {
			console.log("onCompleted args:" + JSON.stringify(Qt.application.arguments));
			dispatcherHandler.handleOnCompleted(Qt.application.arguments);
		}
		urssesMainDB.syncUrls();
	}	
	// -------------------------------------------- UI ----------------------------------------------
	
	AdaptivePageLayout {
		id:mainLayout
		anchors {
			fill:parent
		}
		
		LayoutMirroring.enabled: false
		LayoutMirroring.childrenInherit: true
		
		asynchronous: true
		primaryPage : MainFeedPage {
			id:mainFeed
				model: root.urls
				header: PageHeaderWithBottomText {
					id: _header
					title: i18n.tr('Rsses')
					sectionsModel : []
					property var mode: "regular"
					property var searchHeader : Row {
						anchors {
							verticalCenter:parent.verticalCenter
							left:parent.left
							right:parent.right
							topMargin: units.gu(0.05)
							bottomMargin: units.gu(0.05)
						}
						id:contentsRow
						spacing:units.gu(0.5)
						Button {
							height:parent.height
							width:height
							color:theme.palette.normal.background
							iconName:"cancel"
							onClicked:{
								_header.mode = "regular";
								mainFeed.clearSearch();
							}
						}
						TextField {
							id:searchTextField
							width:parent.width - parent.height - units.gu(2)
							onTextChanged:if(text.length > 3) {
								mainFeed.searchFor(text);
							}
							onAccepted: {
								mainFeed.searchFor(text);
							}
							 Keys.onEscapePressed: {
								focus = false
								_header.mode = "regular";
								mainFeed.clearSearch();
							}
						}
					}
					leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("uRsses")
								iconSource : Qt.resolvedUrl("../assets/logo.svg")
								onTriggered : {
									mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/Information.qml"),{})
								}
							}
						]
					}
					states : [
						State {
							name:"searchMode"
							when: _header.mode == "search"
							PropertyChanges { 
								target: _header;
								trailingActionBar.numberOfSlots : 3;
								contents: _header.searchHeader
							}
						}
					]
					trailingActionBar  { 
						numberOfSlots: 4
						actions : [
								Action {
										text : i18n.tr("Search")
										iconName : 	_header.mode == "search" ? "toolkit_chevron-down_1gu" : "search"
										onTriggered : {
											if(_header.mode ==  "regular" ) {
												_header.mode =   "search";
												searchTextField.focus = true;
											} else {
												mainFeed.searchFor(searchTextField.text,1)
												//if(mainLayout.columns > 1) {
													//mainFeed.selectItem();
												//}
											}
										}
									},
									Action {
										text : i18n.tr("Search Back")
										iconName : 	 "toolkit_chevron-up_1gu"
										visible: _header.mode == "search" 
										onTriggered : {
												mainFeed.searchFor(searchTextField.text,-1)
										}
									},
									Action {
										text : i18n.tr("Add Feed")
										iconName : "list-add"
										onTriggered : {
											mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/AddRssPage.qml"),{})
										}
									},
									Action {
										text : i18n.tr("Settings")
										iconName : "settings"
										onTriggered : {
											mainLayout.addPageToCurrentColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/SettingsPage.qml"),{})
										}
									},
									Action {
										property bool isAscending : appSettings.mainFeedSortAsc == Qt.AscendingOrder;
										text : i18n.tr("Toggle Sorting (%1) ").arg(isAscending ? i18n.tr("Ascending") : i18n.tr("Descending") )
										iconName : isAscending ? "up" : "down"
										onTriggered : {
											appSettings.mainFeedSortAsc = isAscending ?
																			Qt.DescendingOrder :
																			Qt.AscendingOrder;
										}
									}
									
								]
						
					}
					onSectionChanged : {
						mainFeed.currentSection = !sectionIdx ? null : section;
					}
				}
		}
	}
	
	// =============== Sync Next Cloud Feeds ===============
	function syncNextCloud(encodedCreds) {
		root.primaryPageNotify( i18n.tr("Synchronizing nextCloud"));
		root.nextCloudCreds = { 
								host : appSettings.nextCloudCreds.host,
								accountId: appSettings.nextCloudCreds.accountId,
								encodedCreds: encodedCreds
							};
		NextcloudAPI.getFeeds(root.nextCloudCreds, function(feedsResult) {
			if(feedsResult && feedsResult.feeds) {
				var updated = false;// This should prevent reloading of the feeds when there is no need to.
				var nextCloudFeeds = [];
				for(var i in feedsResult.feeds) {
					if( feedsResult.feeds[i] && root.urls.indexOf(feedsResult.feeds[i].url) < 0) {
						root.urls.push(feedsResult.feeds[i].url);
						nextCloudFeeds.push(feedsResult.feeds[i].url);
						updated = true;
					}
				}
				
				if(updated) {
					root.urls = root.urls;
				}
				root.primaryPageNotify(updated ?  i18n.tr("Synchronization done") :  i18n.tr("Synchronized"));
			}
		}, function(failedText) {
			console.log("Failed to synch with NextCloud, Got response: "+ failedText );
			root.primaryPageNotify( i18n.tr("Failed to synchronize with NextCloud Account."));
		});
	}
	
	Connections {
		//TODO : implement this, for some reason the add remove API fails with InternalError from the server...
		target:mainEventBillboard
		enabled: root.nextCloudCreds && NextcloudAPI.isCredentialValid(root.nextCloudCreds);
		onRemoveFeed : {
			console.log("Nextcloud remove feed for : "+ JSON.stringify(feedUrl));
			NextcloudAPI.removeFeed(root.nextCloudCreds, feedUrl);
		}
		onAddFeed : {
				console.log("Nextcloud add feed for : "+ JSON.stringify(feedUrl));
				NextcloudAPI.addFeed(root.nextCloudCreds, feedUrl);
		}
		onBookmarkStory : {
			console.log("Nextcloud bookmark story for : "+ JSON.stringify(story.url));
			NextcloudAPI.bookmarkStory(root.nextCloudCreds, story.url)
		}
		onUnbookmarkStory : {
			console.log("Nextcloud Un-bookmark story for : "+ JSON.stringify(story.url));
			NextcloudAPI.unbookmarkStory(root.nextCloudCreds, story.url)
		}
		onReadStory : {
			console.log("Nextcloud mark as read story for : "+ JSON.stringify(story.url));
			NextcloudAPI.readStory(root.nextCloudCreds, story.url)
		}
	}
	
	Timer {
		id:syncNextCloudTimer
		running: appSettings.nextCloudCreds.accountId !== undefined &&
				 appSettings.nextCloudCreds.accountId && accountsModel.ready
		interval: 30 * 60000
		triggeredOnStart: true
		repeat: true
		onTriggered: {
			accountsModel.authenticate({host:appSettings.nextCloudCreds.host,accountId:appSettings.nextCloudCreds.accountId});
			appSettings.nextCloudCreds.encodedCreds = "";
		}
	}
	
	//==================================  Sharing is caring ===========================
	property var urlMapping: {
		"^https?://" : function(url) {
			//if(pageStack && pageStack.currentPage && pageStack.currentPage.folderModel) {
				//pageStack.currentPage.folderModel.goTo(url);
			//}
			console.log("handling http:// for:"+url);
			for(var i in root.urls) {
				if(url == root.urls[i]) {
					console.log(""+url+ " Allredy exists");
					return;
				}
			}
			root.urls[""+Object.keys(appSettings.urls).length] = url;
			root.primaryPageNotify( i18n.tr("Added Feed : %1").arg(url) );
			mainFeed.updateFeed()
		}
	}

	DispatcherHandler {
			id:dispatcherHandler
	}
	
	Timer {
		id:initial
	}
	
	
	Connections {
		target: ContentHub
		
		onShareRequested: {
			if ( transfer.contentType == ContentType.Links ) {
					for ( var i = 0; i < transfer.items.length; i++ ) {
					if (String(transfer.items[i].url).length > 0 ) {
						var url = transfer.items[i].url;
						for(var i in root.urls) {
						if(url == root.urls[i]) {
								console.log(""+url+ " Allredy exists");
								return;
							}
						}
						root.urls[""+Object.keys(appSettings.urls).length] = url;
						root.primaryPageNotify( i18n.tr("Added Feed : %1").arg(url) );
						console.log("Added Feed : " + url);
						mainFeed.updateFeed()
					}
				}
			}
		}
		onImportRequested: {
			opmlImporter.importOPML(transfer);
		}
	}
	
	OpmlImporter {
		id:opmlImporter
	}
	
	HelperFunctions {
		id:helperFunctions
	}
	
	ShareItem {
		id:shareItem
		parentPage:mainFeed
	}
	

	//===================================== Functions ========================================
	
	function primaryPageNotify(message) {
		if(mainLayout.primaryPage && mainLayout.primaryPage.header && mainLayout.primaryPage.header.message ) {
			mainLayout.primaryPage.header.message.text = message;
		} else	{
			console.log("Failed when trying to notify about : "+ message);
		}
	}
	function getBookMarkedIdx(url) {
		for(var i in appSettings.bookedmarked) {
			if(appSettings.bookedmarked[i] && appSettings.bookedmarked[i].url == url) {
				return i;
			}
		}
		return -1;
	}
	
	function isBookMarked(url) {
		return root.getBookMarkedIdx(url) >=0;
	}
	
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
