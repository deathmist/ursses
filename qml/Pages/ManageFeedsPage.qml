
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls

import "../Components"

Page {
	id:_manageFeed
	
	property var feedsModel: []
	
	clip:true
	
	header: PageHeader {
		id:header
		title:i18n.tr("Manage Feeds")
		trailingActionBar  {
						numberOfSlots:4
						actions : [
							Action {
								text : i18n.tr("Apply")
								iconName : "save"
								onTriggered : {
									for(var feed of root.urls) {
										if( _feedsList.model && feed &&  _feedsList.model.indexOf(feed) < 0) {
											console.log("Remove Feed : "+ feed);
											root.mainBillboard.removeFeed(feed);
										}
									}
									root.urls = _feedsList.model
									mainLayout.removePages(_manageFeed);
									root.primaryPageNotify(i18n.tr("Changes to feed list saved."));
								}
							},
							Action {
								text : i18n.tr("Add Feed")
								iconName : "list-add"
								onTriggered : {
									mainLayout.addPageToNextColumn(_manageFeed, Qt.resolvedUrl("AddRssPage.qml"),{})
								}
							},
							Action {
								enabled:false
							},
							Action {
								text : i18n.tr("Export feeds")
								iconName : "share"
								onTriggered : {
									root.mainBillboard.exportFeeds(_feedsList.model);
								}
							}
						]
		}
	}

	Connections {
		target:root.mainBillboard
		onFeedsChanged: {
			_manageFeed.feedsModel = feeds;
		}
	}
	
	UbuntuListView {
		id:_feedsList
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
		}
		model:_manageFeed.feedsModel
		delegate: ListItem {
			ListItemLayout {
				title.text : modelData
			}
			leadingActions : ListItemActions { 
				actions : Action {
					name: "Delete"
					text: i18n.tr("Delete")
					iconName:'delete'
					onTriggered: {
						_manageFeed.feedsModel.splice(index,1);
						_manageFeed.feedsModel = _manageFeed.feedsModel;
					}
				}
			}
			trailingActions : ListItemActions {
				actions: [
					Action {
					name: "Copy"
					text: i18n.tr("copy")
					iconName:'edit-copy'
					onTriggered: {
						 var mimeData = Clipboard.newData();
						 mimeData.text = modelData
						 mimeData.format = [ "text/uri-list","text/plain" ];
						Clipboard.push(mimeData)
					}
				}
				]
			}
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

