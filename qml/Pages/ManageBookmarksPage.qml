
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls

import "../Components"

Page {
	id:_bookmarksPage
// 	anchors.fill:parent

	property alias model : _bookmarksList.model

	clip:true
	
	header: PageHeader {
		id:header
		
		property var state : "";
		property ActionList regularStateActions : ActionList {
			actions: [
				Action {
					text : i18n.tr("Export")
					iconName : "save-as"
					onTriggered : {
						_bookmarksPage.state = "export";
					}
				}
			]
		}
		property ActionList exportStateActions : ActionList {
			actions :[
				Action {
					text : i18n.tr("OK Export")
					iconName : "ok"
					onTriggered : {
						var items = [];
						for(var i=0; i < _bookmarksList.ViewItems.selectedIndices.length; i++) {
							items.push( _bookmarksList.model[_bookmarksList.ViewItems.selectedIndices[i]]);
						}
						root.mainBillboard.exportBookmarks(_internalFunctions.bookmarksToHTML(items));
						_bookmarksPage.state = "";
					}
				},
				Action {
					text : i18n.tr("Cancel Export")
					iconName : "cancel"
					onTriggered : {
						_bookmarksPage.state = "";
					}
				},
				Action {
					enabled:false
				},
				Action {
					property bool hasSelection: _bookmarksList.ViewItems.selectedIndices.length != 0;
					text : i18n.tr("Toggle Select")
					iconName : !hasSelection ? "select" : "select-none"
					onTriggered : {
						if(hasSelection) {
							_bookmarksList.clearSelection()
						} else  {
							_bookmarksList.selectAll()
						}
					}
				}

			]
		};
		title:i18n.tr("Bookmarks")
		states:[
			State {
				name:"export"
				when:_bookmarksPage.state == "export"
				PropertyChanges { target :header; trailingActionBar.numberOfSlots:4; trailingActionBar.actions : header.exportStateActions.actions }
			},
			State {
				name:"regular"
				when:_bookmarksPage.state == ""
				PropertyChanges { target :header; trailingActionBar.numberOfSlots:3; trailingActionBar.actions : header.regularStateActions.actions	}
			}
		]
	}
	
	Label {
		anchors.centerIn:parent
		visible: _bookmarksList.model.length == 0
		text:i18n.tr("No bookmarks yet…")
	}
	
	ShareItem {
		id:shareItem
		parentPage:mainFeed
	}
	

	states : [
		State {
				name:"export"
				when:_bookmarksPage.state == "export"
				PropertyChanges { target :_bookmarksList; ViewItems.selectMode :true}
			}
	]
	
	UbuntuListView {
		id:_bookmarksList
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
		}
		model:  appSettings.bookedmarked
		delegate:  ListItem {
			height:units.gu(10)
			ListItemLayout {

				title.text : modelData.titleText
				subtitle.text : modelData.url
				summary.text : modelData.description
				summary.textFormat:Text.PlainText
				Icon {
					width:units.gu(3)
					height:width
					name:"go-next"
					SlotsLayout.position: SlotsLayout.Trailing;
				}
			}
			leadingActions : ListItemActions{ 
				actions : Action {
					text: i18n.tr("Delete")
					iconName:'delete'
					onTriggered: if(modelData){
						root.mainBillboard.unbookmarkStory(modelData);
					}else {//Workaround  for the null modelData issue , remove this when it`s fixed
						appSettings.bookedmarked.splice(index,1);
						appSettings.bookedmarked = appSettings.bookedmarked;
					}
				}
			}
			trailingActions : ListItemActions{ 
				actions : [
					Action {
						text: i18n.tr("Share")
						iconName:'share'
						onTriggered: {
							shareItem.shareLink(modelData.url);
							
						}
					},
					Action {
						text: i18n.tr("Open Externally")
						iconName:'external-link'
						onTriggered: {
							Qt.openUrlExternally(modelData.url);
						}
					}
				]
			}
			onClicked: if(!_bookmarksPage.state){
				mainLayout.addPageToNextColumn(	mainLayout.primaryPage, 
												Qt.resolvedUrl("EnteryViewPage.qml"),
												{"model": modelData} );
			}
		}
		
		//------------------------------------------------------------------------------------
		function clearSelection() {
			ViewItems.selectedIndices = []
		}

		function selectAll() {
			var tmp = []
			for (var i=0; i < _bookmarksList.model.length; i++) {
				tmp.push(i)
			}
			ViewItems.selectedIndices = tmp
		}
	}
	QtObject {
		id:_internalFunctions
		
		function bookmarksToHTML(bookmarks) {
			var html = "<DL><DT><H3>uRsses Bookmarks</H3></DT>\n<DL>\n";
			for( var i in bookmarks ) {
				html += "<DT><A HREF='" + bookmarks[i].url + "' >" +  bookmarks[i].titleText + "</A></DT>\n";
			}
			html += "</DL>\n</DL>";
			
			return html;
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

