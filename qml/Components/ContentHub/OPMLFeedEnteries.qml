import QtQuick.XmlListModel 2.0

XmlListModel {
	id:_opmlFeedEntries
	
    query: "/opml/body/outline"
	XmlRole { name: "title"; query: "@title/string()" }
	XmlRole { name: "description"; query: "@description/string()" }
	XmlRole { name: "url"; query: "@xmlUrl/string()" }
	XmlRole { name: "type"; query: "@type/string()" }
}
/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


