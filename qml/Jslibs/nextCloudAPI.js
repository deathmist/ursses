var mapping = {
	"feed_path" : "/index.php/apps/news/api/v1-2/feeds",
	"bkmrk_item_path" : "/index.php/apps/news/api/v1-2/items/__FEEDID__/__GUIDHASH__/star",
	"unbkmrk_feed_path" : "/index.php/apps/news/api/v1-2/items/__FEEDID__/__GUIDHASH__/unstar",
	"add_feed_path" : "/index.php/apps/news/api/v1-2/feeds",
	"remove_feed_path" : "/index.php/apps/news/api/v1-2/feeds/__ID__",
	"read_item_path" : "/index.php/apps/news/api/v1-2/items/__ID__/read",
	"get_items_list" : "/index.php/apps/news/api/v1-2/items"
}

var pathArgMapping = {
	"__ID__" : "id",
	"__GUID__" : "guid",
	"__GUIDHASH__" : "guidHash",
	"__FEEDID__" : "feedId"
}

var lastFeedList = [];
var lastItemsList = [];
var lastCreds = null;

function getFeeds(nextCloudCreds, callback, failback) {
	console.log("NextCloud.js: getFeeds function called.");
	var xhr = new XMLHttpRequest();
	xhr.open("GET",nextCloudCreds.host+mapping['feed_path']);
	xhr = this.addCredsToXhr(xhr,{host : nextCloudCreds.host, encodedCreds: nextCloudCreds.encodedCreds });
	if(!xhr) {//auth failed
		failback(xhr.responseText);
		return false;
	}
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var parsedData = JSON.parse(xhr.responseText);
			callback(parsedData);
			lastFeedList = parsedData.feeds;
			if(lastFeedList) {
				lastCreds = {host : nextCloudCreds.host, encodedCreds: nextCloudCreds.encodedCreds}; 
			}
		} else if(xhr.readyState == 4 && xhr.status !== 200) {
			if(failback) {
				failback(xhr.responseText);
			}
		}
	};
	xhr.onerror = function() {
		failback(xhr.responseText);
	}
	xhr.send();
	this.updateItemsList(nextCloudCreds);
	return  true;
}

function addFeed(nextCloudCreds, newFeed, callback, failback) {
	console.log("NextCloud.js: addFeed function called.");
	var xhr = this.getXHR("POST",nextCloudCreds.host+mapping['add_feed_path'],nextCloudCreds);
	if(!xhr) {//auth failed
		return false;
	}
	xhr.onreadystatechange =this.getHttpHandlerFunc(xhr, callback, failback);
	xhr.send(JSON.stringify({url:newFeed,folderId:null}));
	return  true;
}

function removeFeed(nextCloudCreds, feedToRemove, callback, failback) {
	console.log("NextCloud.js: removeFeed function called.");
	var feedData = this.getFeedByURL(feedToRemove);
	if(!feedData) {  return false }
	
	var xhr = this.getXHR("DELETE",nextCloudCreds.host+relpacePathValues( mapping['remove_feed_path'],feedData),nextCloudCreds);
	if(!xhr) {//auth failed
		return false;
	}
	xhr.onreadystatechange =this.getHttpHandlerFunc(xhr, callback, failback);
	xhr.send();
	return  true;
}

function bookmarkStory(nextCloudCreds, storyUrl, callback, failback) {
	console.log("NextCloud.js: bookmarkStory function called.");
	return  this.itemsManipulation(nextCloudCreds, storyUrl, mapping['bkmrk_item_path'], callback, failback);;
}

function unbookmarkStory(nextCloudCreds, storyUrl, callback, failback) {
	console.log("NextCloud.js: unbookmarkStory function called.");
	return  this.itemsManipulation(nextCloudCreds, storyUrl, mapping['unbkmrk_feed_path'], callback, failback);
}

function readStory(nextCloudCreds, storyUrl, callback, failback) {
	console.log("NextCloud.js: readStory function called.");
	return  this.itemsManipulation(nextCloudCreds, storyUrl, mapping['read_item_path'], callback, failback);
}

//--------------------------------------------------------------------------------------

function isCredentialValid(nextCloudCreds) {
	return nextCloudCreds.host.match(/^https:/) && nextCloudCreds.encodedCreds;
}

//---------------------------------------------------------------------------------------

function itemsManipulation(nextCloudCreds, storyUrl, updatePathMapping, callback, failback) {
	var itemData = this.getItemByURL(storyUrl,nextCloudCreds);
	if(!itemData) {  return false }
	var xhr = this.getXHR("PUT",nextCloudCreds.host+relpacePathValues( updatePathMapping,itemData),nextCloudCreds);
	
	if( !xhr ) {//auth failed
		console.log("NextCloud.js: Authentication failed or other XHR issue.");
		return false;
	}
	xhr.onreadystatechange = this.getHttpHandlerFunc(xhr, callback, failback);
	xhr.send(JSON.stringify({url:storyUrl}));
	return  true;
}

function relpacePathValues(path, feed) {
	for(var i in pathArgMapping) {
			var regex = new RegExp(i);
			if( path.match(regex) && feed[pathArgMapping[i]] ) {
				path = path.replace(regex,feed[pathArgMapping[i]]);
			}
	}
	return path;
}

function getFeedByURL(feedURL) {

	if(typeof( lastFeedList ) === 'object' ) {
		for(var i in lastFeedList) {
			if(lastFeedList[i].url == feedURL ) {
				return lastFeedList[i];
			}
		}
	}
	return null;
}

function getItemByURL(itemURL, nextCloudCreds) {
	if(!this.lastItemsList || !this.lastItemsList.length) {
		this.updateItemsList(nextCloudCreds);
	}
	if(typeof( this.lastItemsList ) === 'object' ) {
		for(var i in this.lastItemsList) {
			if(this.lastItemsList[i].url == itemURL ) {
				return this.lastItemsList[i];
			}
		}
	}
	return null;
}

function updateItemsList(nextCloudCreds) {
	console.log("NextCloud.js: updateItemsList function called.");
	var xhr = this.getXHR("GET",nextCloudCreds.host+mapping['get_items_list'], nextCloudCreds);
	if( !xhr ) {//auth failed
		return false;
	}
	xhr.onreadystatechange = this.getHttpHandlerFunc(xhr, function(data){
		if(data && data.items) {
			lastItemsList = data.items;
		}
	});

	xhr.send();
}

function getXHR(method, url, nextCloudCreds) {
	var xhr = new XMLHttpRequest();
// 	console.log("NextCloud.js: URL : "+url);
	xhr.open(method, url);
	xhr.setRequestHeader("Content-Type","application/json");

	return this.addCredsToXhr(xhr,nextCloudCreds);
}

function addCredsToXhr(xhr,nextCloudCreds) {
	if(!nextCloudCreds.host.match(/^https:/)) {
		console.log("Host is not under SSL! failing instead of exposing credentials... ");
		return false;
	}
	var creds = nextCloudCreds.encodedCreds;
	xhr.setRequestHeader('Authorization', 'Basic '+creds);
	return xhr;
}

function getHttpHandlerFunc(xhr, callback, failback) {
	return function() {
		console.log(xhr.status,xhr.readyState,xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == 200) {
// 			console.log("NextCloud.js: request success , received response : \n" + xhr.responseText);
			if(callback) {
				callback(JSON.parse(xhr.responseText));
			}
		} else if(xhr.readyState == 4 && xhr.status !== 200) {
			if(failback) {
				failback(xhr.responseText);
			}
			console.log("NextCloud.js: request failed , received response : \n" + xhr.responseText);
		}
	};
}
